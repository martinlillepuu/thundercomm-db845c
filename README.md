# Thundercomm DB845c PostmarketOS images

## Description
PostmarketOS images built from pmaports master for mainlining DB845c based devices.

## Sources
- https://gitlab.com/postmarketOS/pmaports/-/tree/master/device/testing/device-thundercomm-db845c
- https://gitlab.com/postmarketOS/pmaports/-/tree/master/device/community/linux-postmarketos-qcom-sdm845
- https://gitlab.com/sdm845-mainline/linux

## DIY
- https://wiki.postmarketos.org/wiki/Pmbootstrap

## Restore stock Agnos for Comma 3
- https://github.com/commaai/agnos/blob/master/README.md

## Authors and acknowledgment
PostmarketOS project
Martin Lillepuu

## License
MIT / GPL 2.0

## Project status
Getting started
